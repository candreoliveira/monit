#!/bin/bash

APP_ENV=production
JOBS_DIR=/home/deploy/Projetos/dicax/source/jobs
RVM_DIR=/usr/local/rvm/bin

$RVM_DIR/rvm in $JOBS_DIR do ruby main.rb -j geocoder,import,geocoder_trigger,update_location -i 1 -e $APP_ENV -n 3