#!/bin/bash

# Atualizar script para:
# https://gist.github.com/shapeshed/1221753
# Colocar sleep para dar tempo do unicorn novo subir antes de matar o old

APP_ENV=production

SOURCE_DIR=/home/deploy/Projetos/dicax/source/dicax
CONFIG_DIR=$SOURCE_DIR/config/unicorn
PID_DIR=$SOURCE_DIR/tmp/pids
RVM_DIR=/usr/local/rvm/bin

UNICORN_EXEC=r192p290_dicax_unicorn

PID=""
UNICORN_STR1=""
UNICORN_STR2=""

if [ -f "$PID_DIR/unicorn.pid" ]; then
    PID=$(cat $PID_DIR/unicorn.pid)

    # Unicorn atual
    UNICORN_STR1=$(ps aux | grep $PID | grep -v grep -m 1 | awk '{ print $11 " " $12 }' )
    UNICORN_STR2=$(ps aux | grep $PID | grep -v grep -m 1 | awk '{ print $11 " " $12 " " $13 }' )
fi

wait_usr2 () {     
    sleep 10

    old_PID=$1
    new_UNICORN_STR1=$(ps aux | grep $old_PID | grep -v grep -m 1 | awk '{ print $11 " " $12 }' )
    new_UNICORN_STR2=$(ps aux | grep $old_PID | grep -v grep -m 1 | awk '{ print $11 " " $12 " " $13 }' )

    if [ "$new_UNICORN_STR1" = "unicorn master" ] && [ "$new_UNICORN_STR2" = "unicorn master (old)" ]; then
	kill -QUIT $old_PID
    else
	wait_usr2 $old_PID
    fi
}

check_running () { 
    if [ ! -f $PID_DIR/unicorn.pid ]; then
	return 0
    fi

    new_PID=$(cat $PID_DIR/unicorn.pid)
    new_UNICORN_STR1=$(ps aux | grep $new_PID | grep -v grep -m 1 | awk '{ print $11 " " $12 }' )
    
    if [ "$new_UNICORN_STR1" = "unicorn master" ]; then
	return 1
    fi

    return 0
}

start_unicorn () {
    $RVM_DIR/$UNICORN_EXEC -c $CONFIG_DIR/$APP_ENV.rb -D -E $APP_ENV
}

wait_and_start () {
    count=$1

    if [ $count -le 15 ]; then
	sleep 3
	
	check_running
	if [ "$?" != "1" ]; then
	    wait_and_start $(( $count + 1 ))
	fi
    else
	# Nao esta rodando por mais de 40s
	start_unicorn
	wait_and_start 1
    fi
}

if [ "$UNICORN_STR1" = "unicorn master" ] && [ "$UNICORN_STR2" = "unicorn master (old)" ]; then
    # Possui uma instancia para ser morta
    kill -QUIT $PID
    wait_and_start 1

elif [ "$UNICORN_STR1" = "unicorn master" ] && [ "$UNICORN_STR2" != "unicorn master (old)" ]; then
    # Esta rodando
    kill -USR2 $PID
    wait_usr2 $PID

else
    # Nao esta rodando
    start_unicorn
    wait_and_start 1
fi
